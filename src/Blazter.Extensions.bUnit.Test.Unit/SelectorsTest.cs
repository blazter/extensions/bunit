﻿using Blazter.Extensions.bUnit.Test.Unit.Support;
using Bogus;
using Bunit;

namespace Blazter.Extensions.bUnit.Test.Unit
{
	public class SelectorsTest
	{
		private IRenderedComponent<TestComponent> CreateCUT(string attribute, string value)
		{
			var context = new TestContext();

			return context.RenderComponent<TestComponent>(options => options.Add(c => c.Attributes, new Dictionary<string, object> { [attribute] = value }));
		}

		[Fact(DisplayName = "[UNIT][SLC-001] - Select by Attribute")]
		public void Selectors_ByAttribute()
		{
			// Arrange
			var attribute = new Faker().Random.String();
			var value = new Faker().Random.String();
			var cut = CreateCUT(attribute, value);

			// Act
			var result = cut.FindAll(Selectors.ByAttribute(attribute, value));

			// Assert
			Assert.NotEmpty(result);
		}

		[Fact(DisplayName = "[UNIT][SLC-002] - Select by TestId")]
		public void Selectors_ByTestId()
		{
			// Arrange
			var id = new Faker().Random.String();
			var cut = CreateCUT("data-TestId", id);

			// Act
			var result = cut.FindAll(Selectors.ByTestId(id));

			// Assert
			Assert.NotEmpty(result);
		}
	}
}
