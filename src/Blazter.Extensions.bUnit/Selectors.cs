﻿namespace Blazter
{
	/// <summary>
	/// Create CSS selector expressions by different attributes.
	/// </summary>
	public static class Selectors
	{
		/// <summary>
		/// Create CSS selector expression by custom attribute.
		/// </summary>
		/// <param name="name">Name of the attribute.</param>
		/// <param name="value">Value of the attribute.</param>
		/// <returns>Created CSS selector expression. (Format: <c>[<paramref name="name"/>="<paramref name="value"/>"]</c>)</returns>
		public static string ByAttribute(string name, string value) => $"[{name}=\"{value}\"]";

		/// <summary>
		/// Create CSS selector expression by <b><i>data-testId</i></b> attribute.
		/// </summary>
		/// <param name="id">Test id of the element to be selected.</param>
		/// <returns>Created CSS selector expression. (Format: <c>[data-testId="<paramref name="id"/>"]</c></returns>
		public static string ByTestId(string id) => ByAttribute("data-testId", id);
	}
}
